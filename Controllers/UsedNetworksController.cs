﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IW.Models;

namespace IW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsedNetworksController : ControllerBase
    {
        private readonly IwContext _context;

        public UsedNetworksController(IwContext context)
        {
            _context = context;
        }

        // GET: api/UsedNetworks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UsedNetwork>> GetUsedNetwork(uint id)
        {
            var usedNetwork = await _context.UsedNetworks.FindAsync(id);

            if (usedNetwork == null)
            {
                return NotFound();
            }

            return usedNetwork;
        }

        // PUT: api/UsedNetworks/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsedNetwork(uint id, UsedNetwork usedNetwork)
        {
            if (id != usedNetwork.Id)
            {
                return BadRequest();
            }

            _context.Entry(usedNetwork).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsedNetworkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UsedNetworks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UsedNetwork>> PostUsedNetwork(UsedNetwork usedNetwork)
        {
            _context.UsedNetworks.Add(usedNetwork);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsedNetwork", new { id = usedNetwork.Id }, usedNetwork);
        }

        // DELETE: api/UsedNetworks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsedNetwork(uint id)
        {
            var usedNetwork = await _context.UsedNetworks.FindAsync(id);
            if (usedNetwork == null)
            {
                return NotFound();
            }

            _context.UsedNetworks.Remove(usedNetwork);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UsedNetworkExists(uint id)
        {
            return _context.UsedNetworks.Any(e => e.Id == id);
        }
    }
}
