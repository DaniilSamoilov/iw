﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IW.Models;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace IW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly IwContext _context;

        public ProfilesController(IwContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<Profile>> GetProfile(string telephone)//+79786757662
        {
            var profile = await _context.Profiles
                .Include(p=> p.Tariff)
                .Include(p=> p.UsedNetworks)
                .FirstOrDefaultAsync(u => u.TelephoneNumber == telephone);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> PutProfile(string telephone, Profile profile)
        {
            if (telephone != profile.TelephoneNumber)
            {
                return BadRequest();
            }

            _context.Entry(profile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await _context.Profiles.FirstOrDefaultAsync(p => p.TelephoneNumber == telephone) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Profile>> PostProfile(Profile profile)
        {
            _context.Profiles.Add(profile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfile", new { id = profile.Id }, profile);
        }

        [HttpGet("ChangeTariff")]
        public async Task<ActionResult> ChangeTariff(string telephone, uint tariff_id)
        {
            var profile = await _context.Profiles.FirstOrDefaultAsync(p => p.TelephoneNumber == telephone);
            if (profile== null)
            {
                NotFound();
            }      
            var tariff = await _context.Tariffs.FirstOrDefaultAsync(t => t.Id == tariff_id);
            if (tariff == null)
            {
                return NotFound();
            }
            profile.TariffId = tariff_id;
            profile.Tariff = tariff;
            var Network = new UsedNetwork
            {
                Profile = profile,
                ProfileId = profile.Id,
                Calls = 0,
                Sms = 0,
                Internet = 0
            };
            _context.UsedNetworks.Add(Network);
            await _context.SaveChangesAsync();
            return Ok(profile);
        }

        [HttpGet("UpdateBalance")]
        public async Task<ActionResult> UpdateBalance(string telephone, decimal value)
        {
            var profile = await _context.Profiles.FirstOrDefaultAsync(p => p.TelephoneNumber == telephone);
            if (profile == null)
            {
                NotFound();
            }
            if (profile.Balance+value <0)
            {
                return Forbid("Insufficient balance");
            }
            profile.Balance += value;
            await _context.SaveChangesAsync();
            return Ok(("New balance {0}",profile.Balance));
        }

        [HttpDelete("DeleteProfile")]
        public async Task<IActionResult> DeleteProfile(string telephone)
        {
            var profile = await _context.Profiles.FirstOrDefaultAsync(p => p.TelephoneNumber == telephone);
            if (profile == null)
            {
                NotFound();
            }

            _context.Profiles.Remove(profile);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProfileExists(uint id)
        {
            return _context.Profiles.Any(e => e.Id == id);
        }
    }
}
