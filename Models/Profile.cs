﻿using System;
using System.Collections.Generic;

namespace IW.Models;

public partial class Profile
{
    public uint Id { get; set; }

    public string TelephoneNumber { get; set; } = null!;

    public decimal Balance { get; set; }

    public uint? TariffId { get; set; }

    public DateTime? NextPayment { get; set; }

    public int? Price { get; set; }

    public virtual Tariff? Tariff { get; set; }

    public virtual ICollection<UsedNetwork> UsedNetworks { get; set; } = new List<UsedNetwork>();
}
