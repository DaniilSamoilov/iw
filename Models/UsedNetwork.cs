﻿using System;
using System.Collections.Generic;

namespace IW.Models;

public partial class UsedNetwork
{
    public uint Id { get; set; }

    public int Calls { get; set; }

    public int Sms { get; set; }

    public int Internet { get; set; }

    public uint ProfileId { get; set; }

    public virtual Profile Profile { get; set; } = null!;
}
