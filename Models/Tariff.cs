﻿using System;
using System.Collections.Generic;

namespace IW.Models;

public partial class Tariff
{
    public uint Id { get; set; }

    public string Title { get; set; } = null!;

    /// <summary>
    /// Если -1 то стоимость за услугу а не лимит
    /// </summary>
    public int Price { get; set; }

    public int Calls { get; set; }

    public int Sms { get; set; }

    public int Internet { get; set; }

    public string Description { get; set; } = null!;

    public byte[]? Image { get; set; }

    public virtual ICollection<Profile> Profiles { get; set; } = new List<Profile>();
}
