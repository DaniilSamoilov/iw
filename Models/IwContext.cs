﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace IW.Models;

public partial class IwContext : DbContext
{
    public IwContext()
    {
    }

    public IwContext(DbContextOptions<IwContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Profile> Profiles { get; set; }

    public virtual DbSet<Tariff> Tariffs { get; set; }

    public virtual DbSet<UsedNetwork> UsedNetworks { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("Server=localhost;Database=iw;UserId=root;Password=root");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Profile>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("profiles");

            entity.HasIndex(e => e.TariffId, "profiles_tariff_id_foreign");

            entity.HasIndex(e => e.TelephoneNumber, "profiles_telephone number_unique").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Balance)
                .HasPrecision(8)
                .HasColumnName("balance");
            entity.Property(e => e.NextPayment)
                .HasColumnType("datetime")
                .HasColumnName("next_payment");
            entity.Property(e => e.Price).HasColumnName("price");
            entity.Property(e => e.TariffId).HasColumnName("tariff_id");
            entity.Property(e => e.TelephoneNumber).HasColumnName("telephone number");

            entity.HasOne(d => d.Tariff).WithMany(p => p.Profiles)
                .HasForeignKey(d => d.TariffId)
                .HasConstraintName("profiles_tariff_id_foreign");
        });

        modelBuilder.Entity<Tariff>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("tariffs");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Calls).HasColumnName("calls");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
            entity.Property(e => e.Image)
                .HasColumnType("blob")
                .HasColumnName("image");
            entity.Property(e => e.Internet).HasColumnName("internet");
            entity.Property(e => e.Price)
                .HasComment("Если -1 то стоимость за услугу а не лимит")
                .HasColumnName("price");
            entity.Property(e => e.Sms).HasColumnName("sms");
            entity.Property(e => e.Title)
                .HasMaxLength(255)
                .HasColumnName("title");
        });

        modelBuilder.Entity<UsedNetwork>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("used network");

            entity.HasIndex(e => e.ProfileId, "used network_profile_id_foreign");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Calls).HasColumnName("calls");
            entity.Property(e => e.Internet).HasColumnName("internet");
            entity.Property(e => e.ProfileId).HasColumnName("profile_id");
            entity.Property(e => e.Sms).HasColumnName("sms");

            entity.HasOne(d => d.Profile).WithMany(p => p.UsedNetworks)
                .HasForeignKey(d => d.ProfileId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("used network_profile_id_foreign");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
